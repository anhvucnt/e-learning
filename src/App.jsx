import React from "react";
import { Router, Switch } from "react-router-dom";
import RouteComponent from "./route-config.jsx";
import history from "./common/history";
import UrlCollect from "./common/url-collect";


//--- Loading
import AppLoading from "./components/loading/loading.view";

//--- Layout
import LayoutDesktopView from "./components/layout/layout.view.jsx";
import LayoutDesktopLogin from "./components/layout/layout.login.jsx";
import HomeDesktop from "./modules/home/home.view.jsx";
import ProfileView from "./modules/profile/profile.view.jsx";
import VideoView from "./modules/video/video.view.jsx";
import BookView from "./modules/book/book.view.jsx";
import QuizView from "./modules/quiz/quiz.view.jsx";
import AssignView from "./modules/assign/assign.view.jsx";
import QnaView from "./modules/qna/qna.view.jsx";
import BoardView from "./modules/board/board.view.jsx";
import LoginView from "./modules/login/login.view.jsx";
//--- Error pages
import ErrorPage500 from './modules/error-page/page-500/page-500';

function App() {
  return (
    <div>
      <AppLoading />

      <Router history={history}>
        <Switch>
          {/* Desktop */}
          <RouteComponent
            exact
            layout={LayoutDesktopLogin}
            component={LoginView}
            path={UrlCollect.Login}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={HomeDesktop}
              path={UrlCollect.Home}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={ProfileView}
              path={UrlCollect.Profile}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={VideoView}
              path={UrlCollect.Video}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={BookView}
              path={UrlCollect.Book}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={QuizView}
              path={UrlCollect.Quiz}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={AssignView}
              path={UrlCollect.Assign}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={QnaView}
              path={UrlCollect.Qna}
          />

          <RouteComponent
              exact
              layout={LayoutDesktopView}
              component={BoardView}
              path={UrlCollect.Board}
          />

          {/* error pages */}
          <RouteComponent
            exact
            layout={LayoutDesktopView}
            component={ErrorPage500}
            path={UrlCollect.Page500 + ":id"}
          />
          {/* lap */}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
