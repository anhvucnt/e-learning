import PlanningCategoriesModel from "../map-data-model-b/planning-categories-model";
export interface GeneralPlanningViewProps {
  GetAllPlanningBoundaries: Function;
  GetAllPlanning: Function;
  GetAllPlanningCategoriesType: Function;
  listPlanningCategoriesAll: PlanningCategoriesModel[];
  location: any;
}

export interface GeneralPlanningViewState {
  isLeftNavbarHide: boolean;
  modalHeightStyle: number;
  isShowMapToolsPanel: boolean;
  isShowFilterInfomationPopup: boolean;
  isDisplaySearchResult: boolean;
  displaySearchLocationBar: boolean;
  coordinate: string;
}
