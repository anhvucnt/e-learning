export interface HomePage {
    avatar:number;
    introduce:string;
    status:number;
    id:number;
    updateOn:string;
    order:number;
    title:string;
}