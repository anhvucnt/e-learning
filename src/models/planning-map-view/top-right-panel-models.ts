import BaseMapModels from "../map-data-model-b/base-map-models";
import OpenlayerController from "../open-layer/openlayer-controller-custom.model";

export interface TopRightPanelPropsModel {
  // coords?: CoordsModel;
  // isGeolocationEnabled?: boolean;
  openLayerController: OpenlayerController;
  mapData: any;
  isHiddenMapTools?: boolean;
  SetDisplayInfomationPopup: Function;
  functionClickViewInfomationListener?: Function;
  SetDisplayMaptoolPanel: Function;
  isMapToolsPanelsDisplay: boolean;
  baseMapList: BaseMapModels.BaseMapModel[];
  isHiddenSearch?: boolean;
  isHiddenLayer?: boolean;
  isHiddenInfor?: boolean;
  planningId?: number;
  setPlanningModelInMap?: Function;
  listBoundaries: any[];
  listPolygon: any;
  GetAllPlanningBoundariesGeomText?: Function;
}
export interface CoordsModel {
  accuracy: number;
  altitude: any;
  altitudeAccuracy: any;
  heading: any;
  latitude: number;
  longitude: number;
  speed: number;
}
export interface PlanningModelMapTool {
  id: number;
  name: string;
  population: number;
  acreage: number;
  numberOfDecisions: number;
}
