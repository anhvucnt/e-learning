import LayerCategoryModels from '../map-data-model-b/layer-category-models';
import PlanningRelationShipModels from '../map-data-model-b/planning-relation-ship-models';
import SingleMapInfoMationModel from '../map-data-model-b/single-map-infomation-model';
import PlanningRelationModel from '../map-data-model-b/new-planning-relation.model';
import BoundariesModel from '../map-data-model-b/new-boundaries.model';
import OpenlayerController from '../open-layer/openlayer-controller-custom.model';
namespace LeftMenuModels {
    export interface LeftMenuProps {
        listGroupLayer: LayerCategoryModels.LayerCategoryModel[];
        toggleStateIsLeftNavbarHide: Function;
        isLeftNavbarHide: boolean;
        listPlanningRelationShips: PlanningRelationModel[];
        informationForTableResult: any[];
        listMapInfomations?: SingleMapInfoMationModel[]
        planningId: number;
        GetListPlanningRelationByPlanningId: Function;
        listPlanningBoundaries: BoundariesModel[];
        openLayerController: OpenlayerController,
        headerTable: any[]
    }
}

export default LeftMenuModels