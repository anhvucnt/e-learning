import MapDataModels from "../map-data-model-b/map-data-model";
import RoutePropsModels from "../route-models/route-models";
import PlanningRelationShipModels from "../map-data-model-b/planning-relation-ship-models";

namespace PlanningMapViewModel {
  export interface PlanningMapViewProps extends RoutePropsModels {
    GetMapDetail: (planningId: number) => any;
    hasLoadedMapData: boolean;
    mapData: MapDataModels.MapDataModels;
    planningRelationShips: PlanningRelationShipModels.PlanningRelationShipModel[];
  }

  export interface PlanningMapViewState {
    mapPlanning: any;
    isLeftNavbarHide: boolean;
    modalHeightStyle: number;
    isShowMapToolsPanel: boolean;
    isShowFilterInfomationPopup: boolean;
    displaySearchLocationBar: boolean;
    planningModel: any;
  }
  // export interface PlanningModelSelectBox {
  //   id: number;
  //   name: string;
  //   mapCenter: string;
  // }
  export interface BoundaryModel {
    id: number;
    name: string;
    mapCenter: string;
  }
}

export default PlanningMapViewModel;
