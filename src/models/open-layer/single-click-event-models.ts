namespace SingleClickEventModels {
  export interface SingleClickEventModel {
    coordinate: [number, number]; 
    coordinate_: any;
    dragging: boolean;
    frameState: any;
    map: any;
  }
}

export default SingleClickEventModels;
