import MapModel from "../open-layer/map";
import LayerSettingsModels from "../map-data-model-b/layer-setting-models";
import ImageLayerModels from "../open-layer/image-layer";
import SingleMapInfoMationModel from "../map-data-model-b/single-map-infomation-model";
import BoundariesModel from "../map-data-model-b/new-boundaries.model";
import Feature from "ol/Feature";
import VectorImageLayer from "ol/layer/VectorImage";
namespace OpenLayerComponent {
  export interface StateType {
    map: MapModel.Map | any;
    mapContainerStyle: any;
    currentLayer: ImageLayerModels.ImageLayerModel | null;
    currentLayerSettingModel: LayerSettingsModels.LayerSettingsModel | null;
    useFeatureViewInfomationOnClick: boolean;
    modals: boolean;
    coordinate: any;
    listLayerSettingModel: LayerSettingsModels.LayerSettingsModel[];
    primaryIdLayer: number;
    isLoadedAllVectorSource: boolean;
  }

  export interface PropsType {
    listMapInfomations?: any;
    planningModel?: any;
    SaveMapBase: any;
    coordinate?: any;
    dataFilter?: any;
    isGeneralPlanning?: boolean;
    projection: string;
    defaultCenter?: number[];
    defaultZoom?: number;
    maxZoom?: number;
    minZoom?: number;
    extent?: [number, number, number, number];
    defaultBaseMapUrl?: string;
    CreateHandleFunction?: Function;
    CreateOpenlayerController: Function;
    functionClickViewInfomationListener?: Function;
    listLayer?: LayerSettingsModels.LayerSettingsModel[];
    mapPlanning?: any;
    mapData?: any;
    SaveInfomationList: (listInfoMation: SingleMapInfoMationModel[]) => any;
    SaveInfomationForTable: (data: any[]) => any;
    SaveHeaderForTable: (data: any[]) => any;
    featuresSelected?: Feature[];
    SaveOpenlayerFeaturesSelected?: (featuresSelected: Feature[]) => any;
    SetDisplayInfomationPopup?: (isDisplay: boolean) => any;
    ShowLoading: () => any;
    HiddenLoading: () => any;
    listPolygon?: any[];
    functionHightLightPolygon?: Function;
    listGroupLayer?: any;
  }
  export interface OutsideHandleFunctionOption {
    type: string;
    params: {
      listLayer?: any;
      groupLayerName?: string;
      layer?: any;
      currentLayerOpacity?: number;
      basemapUrl?: string;
      isDisplay?: boolean;
      boundaries?: BoundariesModel;
    };
  }
}

export default OpenLayerComponent;
