import VectorSource from "ol/source/Vector";
import Draw from "ol/interaction/Draw";
interface DrawnFilterDataModel {
  draw: Draw | null;
  source: VectorSource | null;
  filterType: string;
}

export default DrawnFilterDataModel;
