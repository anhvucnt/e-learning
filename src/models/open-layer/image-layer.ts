import BaseLayerModel from "./base-layer";

namespace ImageLayerModels {
  export interface ImageLayerModel extends BaseLayerModel.BaseLayer {
    className_: any;
    dispatching_: any;
    disposed_: any;
    eventTarget_: any;
    listeners_: any;
    mapPrecomposeKey_: any;
    mapRenderKey_: any;
    ol_uid: any;
    pendingRemovals_: any;
    renderer_: any;
    revision_: any;
    sourceChangeKey_: any;
    state_: any;
    values_: ImageLayerValues_Model;
    getSource: Function;
  }

  export interface ImageLayerValues_Model {
    source: ImageLayerValues_SourceModel;
    maxResolution: any;
    maxZoom: number;
    minResolution: number;
    minZoom: number;
    opacity: number;
    visible: boolean;
    zIndex: number;
  }

  export interface ImageLayerValues_SourceModel {
    params_: {
      LayerId: number; 
      GroupLayer: string;
      LAYERS: string;
    };
  }
}

export default ImageLayerModels;
