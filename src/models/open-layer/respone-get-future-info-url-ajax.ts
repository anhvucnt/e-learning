export default interface ResponeFromServeClickShowInfomationFeature {
  numberReturned: number;
  timeStamp: string;
  totalFeatures: string;
  type: string;
  crs: {
    properties: string; 
    type: string;
  };
  features: FeaturesModel[]; 
}

interface FeaturesModel {
  geometry: {
    coordinates: any[]; 
    type: string;
  };
  geometry_name: string;
  type: string;
  properties: object;
  id:string;
}
