import BoundariesModel from "../map-data-model-b/new-boundaries.model";
import LayerSettingsModels from "../map-data-model-b/layer-setting-models";
import { BoundariesViewModel } from "../map-data-model-b/new-planning-relation.model";
import GetAllPlanningBoundariesMode from "../map-data-model-b/get-all-planning-boundaries.model";
interface OpenlayerController {
  functionClickViewInfomationListener: any;
  changeBaseMap: (newBasemapUrl: string) => any;
  changeCurrentLayerOpacity: (
    opacity: number,
    layer?: LayerSettingsModels.LayerSettingsModel
  ) => any;
  changeCurrentLayer: (layer: LayerSettingsModels.LayerSettingsModel) => any;
  toggleDisplayLayer: (layer: LayerSettingsModels.LayerSettingsModel) => any;
  handleZoomIn: () => any;
  handleZoomOut: () => any;
  handleSearchCoordinate: (event: any) => any;
  placeSearch: (event: any) => any;
  handleMeasureMode: (isTurnOn: boolean, isLineMode: boolean) => any;
  handlePinMarkerMode: (isTurnOn: boolean) => any;
  handleDisplayFullScreenViewMode: (isFullScreen: boolean) => any;
  handleOnOffViewInfomation: (isTurnOn: boolean) => any;
  toggleDisplayVectorLayer: (
    boundaries: BoundariesModel,
    isDisplay: boolean,
    minZoom: number,
    maxZoom: number,
    tooltipValue: string,
    planningId: number
  ) => any;
  toggleDisplayBoundariesRelation: (
    boundaries: BoundariesViewModel,
    isDisplay: boolean
  ) => any;
  functionDisplayAllBoundariesOfAllPlanning: (
    data: GetAllPlanningBoundariesMode[]
  ) => any;
  functionTurnOffFilterMode: () => any;
  clearFeaturesSelected: () => any;
  filterDrawnVectorLayer: (_drawType: string) => void;
  deleteFilterDrawnVectorLayer: () => void;
  getCurrentDrawFeature: () => void;
}

export default OpenlayerController;
