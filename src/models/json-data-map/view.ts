namespace ViewModel {
  export interface view {
    center: number[];
    extent: [number, number, number, number]; 
    maxZoom: number;
    minZoom: number;
    name: string; 
    projection: string;
    zoom: number;
  }
}

export default ViewModel;
