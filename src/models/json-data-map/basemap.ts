namespace BaseMapModel {
  export interface BaseMap {
    checked: boolean;
    id: string;
    layertype: string;
    text: string; 
    texten: string;
    type: string;
    url: string;
    zindex: number;
  }
}

export default BaseMapModel;
