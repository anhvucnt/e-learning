namespace LayerJson {
    export interface Layer {
      
      id: string; 
      index: number;
      level: number;
      pid: string; 
      text: string; 
      type: string; 
      checked?: boolean; 
      filter?: LayerFilter;
      layerid?: string;
      layertype?: string;
      style?: any;
      table?: string;
      wms?: string;
      wmsExternal?: boolean;
      zindex?: string;
      viewparams?: any;
      display?: LayerDisplay;
    }
  
    export interface LayerDisplay {
      cols: any[];
      popup: any;
      tooltip: any;
      viewdetail: any;
    }
  
    export interface LayerFilter {
        in: any[];
        order:string;
        out: any[]
    }
  }
  
  export default LayerJson;
  