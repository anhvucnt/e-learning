import BasemapModel from "./basemap";
import LayersModel from "./layers";
import ViewModel from "./view";

export default interface JSONDATA {
  basemap: BasemapModel.BaseMap;
  layers: LayersModel.Layer;
  view: ViewModel.view;
}
