interface RoutePropsModels {
  history: any;
  location: {
    pathname: string;
  };
  match: {
    params: any; // contain params object
    path: string;
    isExact: boolean;
    url: string;
  };
}

export default RoutePropsModels;
