import MapDataModels from "./map-data-model";
import PlanningRelationShipModels from "./planning-relation-ship-models";

interface JsonGetPlanningByIdModel {
  id: number; 
  name: string;
  planningTypeId: number;
  planningTypeName: string;
  photo: string | null;
  mapModel: MapDataModels.MapDataModels | null;
  planningRelationShipModels: PlanningRelationShipModels.PlanningRelationShipModel[];
  nameTb: string;
  geomText: string;
}

export default JsonGetPlanningByIdModel;
