import MapDataModels from "./map-data-model";

namespace PlanningRelationShipModels {
  export interface PlanningRelationShipModel {
    planningId: number;
    planningMapModels: PlanningMapModels[];
    planningTypeName: string; 
    planningTypeId: number;
  }

  export interface PlanningMapModels {
    planningName: string;
    mapModel: MapDataModels.MapDataModels;
  }
}

export default PlanningRelationShipModels;
