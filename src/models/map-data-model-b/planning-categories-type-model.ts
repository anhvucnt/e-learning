export default interface PlanningCategoriesTypeModel {
  id: number;
  name: string;
}
