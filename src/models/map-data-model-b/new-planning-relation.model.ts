interface PlanningRelationModel {
  planningId: number;
  planningTypeId: number;
  planningTypeName: string;
  planningName: string;
  boundariesId: number;
  planningMapModels: PlanningMapModels[];
}

export interface PlanningMapModels {
  planningName: string;
  boundariesView: BoundariesViewModel;
}

export interface BoundariesViewModel {
  geomText: string;
  id: number;
  nameTb: string;
  tiffName:string;
}

export default PlanningRelationModel;
