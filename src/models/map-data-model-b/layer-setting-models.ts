namespace LayerSettingsModels {
  export interface LayerSettingsModel {
    geo_layer_name: string;
    id: number;
    is_check: boolean;
    layer_type: string;
    layer_category_id: number;
    level: number;
    max_zoom: number;
    min_zoom: number;
    name: string;
    table: string;
    wms: string;
    wms_external: boolean;
    z_index: number;
    display: LayerSettingsDisplayModel;
    filter: LayerSettingsFilterModel;
    display_name: LayerSettingsDisplayModel;
    filter_name: LayerSettingsFilterModel;
    opacity: string;
  }

  export interface LayerSettingsFilterModel {
    order: string;
    in: LayerSettingsFilterInModel[];
    out: LayerSettingsFilterInModel[];
  }

  export interface LayerSettingsFilterInModel {
    col: string;
    alias: string;
    kieu: string;
  }

  export interface LayerSettingsDisplayModel {
    viewdetail: LayerSettingsDisplayViewdetailModel;
    popup: LayerSettingsDisplayViewdetailModel;
    tooltip: LayerSettingsDisplayViewdetailModel;
    cols: LayerSettingsDisplayColModel[];
  }

  export interface LayerSettingsDisplayColModel {
    col: string;
    alias: string;
    index: number;
    kieu: string;
  }

  export interface LayerSettingsDisplayViewdetailModel {
    use: boolean;
    cols: string[];
  }
}

export default LayerSettingsModels;
