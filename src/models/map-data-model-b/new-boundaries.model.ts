interface BoundariesModel {
  nameTb: string;
  geomText: string; 
  id: number;
}

export default BoundariesModel;
