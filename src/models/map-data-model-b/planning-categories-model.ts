import PlanningCategoriesTypeModel from "./planning-categories-type-model";
import JsonGetPlanningByIdModel from "./json-get-planning-by-id-model";
export default interface PlanningCategoriesModel
  extends PlanningCategoriesTypeModel { 
  plannings: JsonGetPlanningByIdModel[];
  minZoom: number;
  maxZoom: number;
}
