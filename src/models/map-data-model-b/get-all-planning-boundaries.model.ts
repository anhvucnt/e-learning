interface GetAllPlanningBoundariesMode {
  id: number;
  name: string;
  photo: string;
  code: string;
  location: string;
  agencySubmitted: string;
  planningUnit: string;
  planningAgency: string;
  consultingUnit: string;
  population: number;
  acreage: number;
  report: string;
  landForConstruction: number;
  nameTable: string;
  geomText: string;
  publishDate: string;
  planningTypeId: number;
  planningTypeName: string;
  minZoom: number; // custom all in FE
  maxZoom: number; // custom all in FE
  zIndex: number;// custom all in FE
}

export default GetAllPlanningBoundariesMode;
