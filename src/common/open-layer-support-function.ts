// import ImageWMS from "ol/source/ImageWMS";
// import { Image as ImageLayer } from "ol/layer";
import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';

export const CreateImageWMSLayer = (
  urlLayer: string,
  idLayer: number,
  minZoom: number,
  maxZoom: number,
  zIndex: number,
  tableName: string,
  visible: boolean = false
) =>
  new TileLayer({
    visible: visible,
    zIndex: zIndex,
    minZoom: minZoom,
    maxZoom: maxZoom,
    source: new TileWMS({
      url: urlLayer,
      params: {
        LAYERS: `caobang:${tableName}`,
        LayerId: idLayer,
        FORMAT: "image/png",
          VERSION: "1.1.0",
        tiled:true
      },
      crossOrigin: 'anonymous'
    }),
  });
