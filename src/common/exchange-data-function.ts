import LayerJson from "../models/json-data-map/layers";
import LeftmenuModel from "../models/open-layer/left-menu";

export function ExchangeLayersDataToStandardData(
  layersData: LayerJson.Layer[]
): LeftmenuModel.ListLayer[] {
  let result: LeftmenuModel.ListLayer[] = [];
  const listLayers:any[] = [];

  layersData.map((layerObj) => {
    if (layerObj.type != "folder") listLayers.push(layerObj);
    else
      result.push({
        groupName: layerObj.text,
        groupId: layerObj.id,
        listItems: [],
      });
  });

  listLayers.map((layer) => {
    result.map((folder) => {
      if (folder.groupId === layer.pid) {
        folder.listItems.push({
          name: layer.text,
          checked: layer.checked
        })
        return true;
      }
    })
  })

  return result;
}
