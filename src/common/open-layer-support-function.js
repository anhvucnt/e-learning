"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateImageWMSLayer = void 0;
// import ImageWMS from "ol/source/ImageWMS";
// import { Image as ImageLayer } from "ol/layer";
var Tile_1 = require("ol/layer/Tile");
var TileWMS_1 = require("ol/source/TileWMS");
exports.CreateImageWMSLayer = function (urlLayer, idLayer, minZoom, maxZoom, zIndex, tableName, visible) {
    if (visible === void 0) { visible = false; }
    return new Tile_1.default({
        visible: visible,
        zIndex: zIndex,
        minZoom: minZoom,
        maxZoom: maxZoom,
        source: new TileWMS_1.default({
            url: urlLayer,
            params: {
                LAYERS: "caobang:" + tableName,
                LayerId: idLayer,
                FORMAT: "image/png",
                VERSION: "1.1.0",
                tiled: true
            },
            crossOrigin: 'anonymous'
        }),
    });
};
//# sourceMappingURL=open-layer-support-function.js.map