const UrlCollect = {
  Login: "/",
  Home: "/home",
  Profile: "/profile",
  Video: "/video",
  Book: "/book",
  Quiz: "/quiz",
  Assign: "/assign",
  Qna: "/qna",
  Board: "/board",
  //-- Error
  Page500: '/page-500/',

  //-- Admin
  ProcessBoardPlanning:"/quy-trinh-cong-bo-quy-hoach"
};

export default UrlCollect