/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../../core/app.store";
import {
  getUserInfo,
} from "../../utils/configuration";
import * as profileActions from "../../redux/store/profile/profile.store";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./profile.scss";

function ProfileView(props) {
  const { showLoading } = props;

    const isLogin = getUserInfo() ? true : false;
    const [profileDetail, setProfileDetail] = useState({});
    const [classInfo, setClassInfo] = useState({});
    //const {id} = props.match.params;
    const id = 1;

    useEffect(() => {
        onGetData();
    }, []);

    const onGetData = () => {
        showLoading(true);
        Promise.all([
            getProfile(id),
            getClassInfo(),
        ])
            .then((res) => {
                showLoading(false);
            })
            .catch((err) => {
                showLoading(false);
            });
    };

    const getProfile = (id) => {
        return new Promise((resolve,reject) => {
            /*
            * We will you axios to call api, get data and store in redux            *
            * */
            /*profileActions.GetDetailProfile(id).then(
                (res) => {
                    setProfileDetail(
                        res && res.content
                    );
                    resolve(res);
                },
                (err) => {
                    reject(err);
                }
            );*/

            /*So here just emulator data response from API*/

            const profile = {
                'Student_ID': 'yoona951023',
                'Name':'YOONA PARK',
                'Gender':'Male',
                'Active':'Active',
                'Birthday':'1995-10-23',
                'Email':'yoona951023@gmail.com',
                'Address':'This is address',
                'Province':'This is province',
                'City':'This is city',
                'Phone1':'111111111111111',
                'Phone2':'22222222222222',
                'Postal_Code':'90000000',
            }
            setProfileDetail(profile);
            resolve(profile);
        })
    }

    const getClassInfo = () => {
        return new Promise((resolve,reject) => {
            const myClass = {
                'Program': 'TESOL with TEC (190 hours)',
                'Period': '2020-05-11 ~ 2020-08-03',
                'Class_Weeks':'12 Weeks',
                'Register_Date':'2020-05-11',
                'ClassList': [
                    {
                        'ClassName':'TESOL Module 01. Introduction',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Score':'7/10',
                        'Assignment_Score':'8/10',
                    },
                    {
                        'ClassName':'TESOL Module 02. Introduction',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Score':'7/10',
                        'Assignment_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 03. Introduction',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Score':'7/10',
                        'Assignment_Score':'6/10',
                    }
                ]
            }
            setClassInfo(myClass);
            resolve(myClass);
        })
    }

  return (
      <div>
            <table width='100%' cellPadding="10" cellSpacing="0" border="0">
                <tbody>
                <tr>
                    <td>
                        <table width="100%" cellPadding='5' cellSpacing='0' className="my-class-box-1">
                            <tbody>
                            <tr>
                                <td height="35" className="cgray">Home &gt; My information &gt; STUDENT MY INFORMATION
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div className="sub_title sub-title-box">My INFORMATION</div>
                        {profileDetail  && (
                        <table width="100%" align="center" cellPadding="10" cellSpacing="0">
                            <tbody>
                            <tr>
                                <td>
                                    <table className="table-margin" width="100%" align="center" cellPadding="0"
                                           cellSpacing="0">
                                        <tr>
                                            <td width="140" valign="top" align="center"
                                                className="lian_bottom lian_top lian_left lian_right">
                                                <table cellPadding="0" border="0" align="center" width="130"
                                                       height="100%">
                                                    <tr>
                                                    </tr>
                                                </table>

                                            </td>
                                            <td>
                                                <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                                    <tr>
                                                        <td width="130"
                                                            className="lian_bottom lian_right lian_top table_blue"
                                                            align="left"><b>Student ID </b></td>
                                                        <td className="lian_bottom lian_top lian_right">
                                                            {profileDetail.Student_ID}
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                                    <tr>
                                                        <td width="130" className="lian_bottom lian_right table_blue"
                                                            align="left"><b> Name </b></td>
                                                        <td className="lian_bottom lian_right">
                                                            {profileDetail.Name}
                                                        </td>

                                                    </tr>
                                                </table>
                                                <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                                    <tr>

                                                        <td width="130" className="lian_bottom lian_right table_blue"
                                                            align="left"><b>Gender </b></td>
                                                        <td className="lian_bottom lian_right">
                                                            {profileDetail.Gender}
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                                    <tr>
                                                        <td width="130" className="lian_bottom lian_right table_blue"
                                                            align="left"><b>Active </b></td>
                                                        <td className="lian_bottom lian_right">
                                                            {profileDetail.Active}
                                                        </td>

                                                    </tr>
                                                </table>
                                                <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                                    <tr>
                                                        <td width="130" className="lian_bottom lian_right table_blue"
                                                            align="left"><b>Birthday </b></td>
                                                        <td className="lian_bottom lian_right">
                                                            {profileDetail.Birthday}
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                                    <tr>
                                                        <td width="130" className="lian_bottom lian_right table_blue"
                                                            align="left"><b>Email </b></td>
                                                        <td className="lian_bottom lian_right">
                                                            {profileDetail.Email}
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                        <tr>
                                            <td width="130"
                                                className="lian_bottom lian_right lian_top lian_left table_blue"
                                                align="left"><b>Address</b></td>
                                            <td width="300" className="lian_bottom lian_top">
                                                {profileDetail.Address}
                                            </td>
                                            <td width="130"
                                                className="lian_bottom lian_right lian_top lian_left table_blue"
                                                align="left"><b>City </b></td>
                                            <td className="lian_bottom lian_top lian_right">
                                                {profileDetail.City}
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                        <tr>
                                            <td width="130" className="lian_bottom lian_right lian_left table_blue"
                                                align="left"><b>Province </b></td>
                                            <td width="300" className="lian_bottom lian_right">
                                                {profileDetail.Province}
                                            </td>

                                            <td width="130" className="lian_bottom lian_right table_blue" align="left">
                                                <b>Postal Code </b></td>
                                            <td className="lian_bottom lian_right">
                                                {profileDetail.Postal_Code}
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" align="center" cellPadding="7" cellSpacing="0">
                                        <tr>
                                            <td width="130" className="lian_bottom lian_right lian_left table_blue"
                                                align="left"><b>Phone1 </b></td>
                                            <td width="300" className="lian_bottom lian_right">
                                                {profileDetail.Phone1}
                                            </td>
                                            <td width="130" className="lian_bottom lian_right table_blue" align="left">
                                                <b>Phone2 </b></td>
                                            <td className="lian_bottom lian_right">
                                                {profileDetail.Phone2}
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" align="center" cellPadding="0" cellSpacing="0" border="0">
                                        <tr>
                                            <td align="center"><a
                                                href="./student_mypage_insert?w=u&page=&REC_KEY=2779"
                                                onFocus="blur();"><img src="images/btn_mod.gif"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        )}
                        <div className="sub_title my-class-box">My class information</div>
                        {classInfo  && classInfo.ClassList && (
                        <table width="100%" align="center" cellPadding="10" cellSpacing="0">
                            <tbody>
                            <tr>
                                <td>
                                    <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                        <tr>
                                            <td width="150" height="40"
                                                className="lian_left lian_right lian_top lian_bottom" align="left"
                                                bgcolor="#d8d5ef"><b>Program</b></td>
                                            <td className="lian_bottom  lian_top lian_right" align="left"
                                                bgcolor="#d8d5ef"><b>{classInfo.Program}</b>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                        <tr>
                                            <td width="150" className="lian_bottom lian_right lian_left table_blue">
                                                <b>Period </b></td>
                                            <td className="lian_bottom lian_right">{classInfo.Period}</td>
                                        </tr>
                                        <tr>
                                            <td width="150" className="lian_bottom lian_right lian_left table_blue"><b>Class
                                                Weeks </b></td>
                                            <td className="lian_bottom lian_right">{classInfo.Class_Weeks}</td>
                                        </tr>
                                        <tr>
                                            <td width="150" className="lian_bottom lian_right lian_left table_blue"><b>Register
                                                Date </b></td>
                                            <td className="lian_bottom lian_right">{classInfo.Register_Date}</td>
                                        </tr>
                                        <tr>
                                            <td width="150" className="lian_bottom lian_right lian_left table_blue">
                                                <b>Class </b></td>
                                            <td className="lian_bottom lian_right">

                                                <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                                    <tr>
                                                        <td className="table_green_t"><b>ClassName</b></td>
                                                        <td className="table_green_t"><b>Class Start</b>
                                                        </td>
                                                        <td className="table_green_t"><b>Class End</b></td>
                                                        <td className="table_green_t"><b>Quiz Score</b></td>
                                                        <td className="table_green_t"><b>Assignment Score</b></td>
                                                    </tr>
                                                    {classInfo.ClassList && (
                                                        classInfo.ClassList.map((item,index) => (
                                                            <tr id={index}>
                                                                <td className='lian_green_l'>{item.ClassName}</td>
                                                                <td className='lian_green_l'>{item.Class_Start}</td>
                                                                <td className='lian_green_l'>{item.Class_End}</td>
                                                                <td className='lian_green_l'>{item.Quiz_Score}</td>
                                                                <td className='lian_green_l'>{item.Assignment_Score}</td>
                                                            </tr>
                                                        ))
                                                    )}
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        )}
                    </td>
                </tr>
                </tbody>
            </table>
	  </div>
  );
}

const mapStateToProps = (state) => ({
    isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps)(ProfileView));
