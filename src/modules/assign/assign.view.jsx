/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { geolocated } from "react-geolocated";
import { AsyncPaginate } from "react-select-async-paginate";
import * as appActions from "../../core/app.store";
import UrlCollect from "../../common/url-collect";
import history from "../../common/history";
import {
  APIUrlDefault,
  changeAlias,
  getUserInfo,
  NotificationMessageType,
  NotificationPosition,
} from "../../utils/configuration";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./assign.scss";

function AssignView(props) {
  const { showLoading } = props;

    const isLogin = getUserInfo() ? true : false;
    const [classInfo, setClassInfo] = useState({});

    useEffect(() => {
        onGetData();
    }, []);

    const onGetData = () => {
        showLoading(true);
        Promise.all([
            getClassInfo(),
        ])
            .then((res) => {
                showLoading(false);
            })
            .catch((err) => {
                showLoading(false);
            });
    };

    const getClassInfo = () => {
        return new Promise((resolve,reject) => {
            const myClass = {
                'Program': 'TESOL with TEC (190 hours)',
                'Period': '2020-05-11 ~ 2020-08-03',
                'Class_Weeks':'12 Weeks',
                'ClassList': [
                    {
                        'ClassName':'TESOL Module 01. Introduction',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 02. The Communicative Approach and Lesson Plan',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 03. Role-Plays and Information Gap',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 04. Teaching Reading',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 05. Teaching Writing',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 05. Teaching Writing',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 06. Teaching Grammar',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 07. Culture in the Class',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 08. Teaching Idioms',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 09. General tips for Teaching English',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 10. Teaching Resume',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 01. Phonics Approach to Teaching Reading',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 02. Whole Language Approach to Teaching Reading',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 03. Child Language Acquisition',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 04. The Classroom Environment',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'202Score0-09-11',
                        'Reg_Date':'2020-08-18',
                        '':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 05. Classroom Management',
                        'AssignUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'DownloadUrl':'http://whisung-chung.com/school/student/download?bo_table=km_class_tbl&wr_id=21&no=0',
                        'DownloadName':'assignment1.doc',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Reg_Date':'2020-08-18',
                        'Score':'3/10',
                    }
                ]
            }
            setClassInfo(myClass);
            resolve(myClass);
        })
    }

    const getTableRow = (item) => {
        const getPopupInfo = (item) => {
            return "javascript:popup_window('"+item.AssignUrl+"','QUIZ','left=0, top=0, width=800, height=650, scrollbars=2,status=0,resizable=0');";
        }

        const getFileDownload = (item) => {
            return "javascript:file_download('"+item.DownloadUrl+"', 'Assignment_Module1.doc');";
        }

        return (
            <tr>
                <td className='lian_green_l lian_green_b'>
                    {item.ClassName}
                </td>
                <td className='lian_green_b'>{item.Class_Start}</td>
                <td className='lian_green_b'>{item.Class_End}</td>
                <td className='lian_green_b'>
                    <img src='images/assign_doc.gif' align="absmiddle"/>
                    <a href={getFileDownload(item)} title=''>
                        {item.DownloadName}
                    </a>
                </td>
                <td className='lian_green_b '>
                    {item.Reg_Date}
                </td>
                <td className='lian_green_b '>
                    <a href={getPopupInfo(item)}>
                        <img src="images/btn_ass.gif"/></a></td>
                <td className='lian_green_b lian_green_r'>
                    {item.Score}
                </td>
            </tr>
        )
    }

  return (
      <table width='100%' cellPadding="10" cellSpacing="0" border="0">
          <tr>
              <td>
                  <table width="100%" cellPadding='5' cellSpacing='0' className="my-class-box-1">
                      <tr>
                          <td height="35" className="cgray">Home &gt; My Class &gt; Online Video Assignment</td>
                      </tr>
                  </table>
                  <div className="sub_title my-class-box">Online Video Assignment</div>
                  {classInfo  && classInfo.ClassList && (
                  <table width="100%" align="center" cellPadding="10" cellSpacing="0">
                      <tr>
                          <td>
                              <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right lian_top table_blue">
                                          <b>Program</b></td>
                                      <td className="lian_bottom  lian_top lian_right">
                                          {classInfo.Program}
                                      </td>
                                  </tr>
                              </table>
                              <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right table_blue">
                                          <b>Period </b></td>
                                      <td className="lian_bottom lian_right">
                                          {classInfo.Period}
                                      </td>
                                  </tr>
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right table_blue"><b>Class
                                          Weeks </b></td>
                                      <td className="lian_bottom lian_right">
                                          {classInfo.Class_Weeks}
                                      </td>
                                  </tr>

                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right table_blue">
                                          <b>Class </b></td>
                                      <td className="lian_bottom lian_right">

                                          <table width='100%' cellPadding='10' cellSpacing='0' align="center">
                                              <tr>
                                                  <td className="table_green_t"><b>ClassName</b></td>
                                                  <td className="table_green_t"><b>Class Start</b></td>
                                                  <td className="table_green_t"><b>Class End</b></td>
                                                  <td className="table_green_t"><b>Download</b></td>
                                                  <td className="table_green_t"><b>Reg Date</b></td>
                                                  <td className="table_green_t"><b>Report</b></td>
                                                  <td className="table_green_t"><b>Score</b></td>
                                              </tr>
                                              {classInfo.ClassList && (
                                                  classInfo.ClassList.map((item,index) => {
                                                      return getTableRow(item);
                                                  })
                                              )}
                                          </table>
                                      </td>
                                  </tr>
                              </table>

                          </td>
                      </tr>
                  </table>
                  )}
              </td>
          </tr>
      </table>
  );
}

const mapStateToProps = (state) => ({
    isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps) (AssignView));
