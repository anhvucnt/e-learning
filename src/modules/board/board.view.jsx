/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { geolocated } from "react-geolocated";
import { AsyncPaginate } from "react-select-async-paginate";


import * as appActions from "../../core/app.store";

import UrlCollect from "../../common/url-collect";
import history from "../../common/history";
import {
  APIUrlDefault,
  changeAlias,
  getUserInfo,
  NotificationMessageType,
  NotificationPosition,
} from "../../utils/configuration";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./board.scss";

function BoardView(props) {
  const { showLoading } = props;

	const isLogin = getUserInfo() ? true : false;
	const [classInfo, setClassInfo] = useState({});

	useEffect(() => {
		onGetData();
	}, []);

	const onGetData = () => {
		showLoading(true);
		Promise.all([
			getClassInfo(),
		])
			.then((res) => {
				showLoading(false);
			})
			.catch((err) => {
				showLoading(false);
			});
	};

	const getClassInfo = () => {
		return new Promise((resolve,reject) => {
			const myClass = {
				'Total': '4',
				'ClassList': [
					{
						'NoticeTitle':'Whisung-Chung Dahm E-Learning',
						'NoticeUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
						'NoticeContent':'Whisung-Chung Dahm E-Learning information'
					},
					{
						'NoticeTitle':'Our New Program: Business and Development',
						'NoticeUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
						'NoticeContent':'What is Business and Development? The Business and Development Program at Whisung-Chung Dahm E-Learning is designed to hel'
					},
					{
						'NoticeTitle':'New Program: Diploma in Translation and Interpretation',
						'NoticeUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
						'NoticeContent':'DTI(Diploma in Translation & Interpretation) is designed to provide in-depth knowledge and skill for bilingual individuals to '
					},
					{
						'NoticeTitle':'Diploma',
						'NoticeUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
						'NoticeContent':'The Whisung-Chung Dahm E-Learning\'s TESOL Course will equip you with the theory and skills to teach English as a second language at a number of different levels'
					}
				]
			}
			setClassInfo(myClass);
			resolve(myClass);
		})
	}

	const getTableRow = (item) => {
		return (
			<tr id={item.index}>
				<td className="subject" height="120" valign="top">
					<nobr className="link-subject">
						<b className="linksubject">
							<a href={item.NoticeUrl}>
								{item.NoticeTitle}
							</a>
						</b>
						<br/>
						{item.NoticeContent}
					</nobr>
				</td>
			</tr>
		)
	}

  return (
		<table width='100%' cellpadding="10" cellspacing="0" border="0" >
			<tr>
				<td>
					Online Class Notice
					{classInfo  && classInfo.ClassList && (
					<table width="97%" align="center" cellpadding="0" cellspacing="0"><tr><td>
								<div class="board_top">
									<div className="float-left">
										<form name="fcategory" method="get" className="margin-0">
										</form>
									</div>
									<div className="float-right">
										<img src="images/icon_total.gif" align="absmiddle" border='0'/>
										&nbsp;
										<span className="total-num">Total {classInfo.Total}</span>
									</div>
								</div>
								<form name="fboardlist" method="post">
									<input type="hidden" name='bo_table' value='board_06' />
									<input type="hidden" name='sfl'  value='' />
									<input type="hidden" name='stx'  value=''/>
									<input type="hidden" name='spt'  value=''/>
									<input type="hidden" name='page' value='1'/>
									<input type="hidden" name='sw'   value=''/>

									<table cellSpacing="0" cellPadding="10" className="board_list">
										{classInfo.ClassList && (
											classInfo.ClassList.map((item,index) => {
												return getTableRow(item);
											})
										)}
									</table>

								</form>

								<div class="board_button">
									<div className="float-left">
									</div>

									<div className="float-right">
									</div>
								</div>

								<div class="board_page">
								</div>
							</td></tr></table>
					)}
				</td>
			</tr>
		</table>
  );
}

const mapStateToProps = (state) => ({
	isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			showLoading: appActions.ShowLoading,
		},
		dispatch
	);

export default (connect(mapStateToProps, mapDispatchToProps)(BoardView));
