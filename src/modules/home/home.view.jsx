/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { geolocated } from "react-geolocated";
import { AsyncPaginate } from "react-select-async-paginate";


import * as appActions from "../../core/app.store";

import UrlCollect from "../../common/url-collect";
import history from "../../common/history";
import {
  APIUrlDefault,
  changeAlias,
  getUserInfo,
  NotificationMessageType,
  NotificationPosition,
} from "../../utils/configuration";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./home.scss";

function HomeDesktop(props) {
  const { showLoading } = props;

  const isLogin = getUserInfo() ? true : false;

  return (
      <table width='100%' cellpadding="10" cellspacing="0" border="0" >
        <tr>
          <td>
            <table width="730" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td align="center">
                    <img src="images/ccmain_top1.gif" />
                  </td>
                </tr>
                <tr>
                  <td align="center">
                    <a href="/book" ><img src="images/bt_01.gif"  border="0" name="image1" /></a>
                    <a href="/video"><img src="images/bt_02.gif"  border="0" name="image2" /></a>
                    <a href="/assign"><img src="images/bt_03.gif"  border="0" name="image3" /></a>
                    <a href="/quiz"><img src="images/bt_04.gif"  border="0" name="image4" /></a>
                  </td>
                </tr>
                </table>
          </td>
        </tr>
      </table>
  );
}

const mapStateToProps = (state) => ({
    isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps) (HomeDesktop));
