/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../../core/app.store";
import { useForm } from "react-hook-form";
import Cookies from 'universal-cookie';
import "./login.scss";

function LoginView(props) {
    const { register,handleSubmit,errors} = useForm({ mode: "onBlur" });
    const cookies = new Cookies();
    const onSubmit = (data) => {
        cookies.set('userInfo', data);
        props.history.push('/home');
    };

    return (
        <div className="login-box">
            <div className="login-left">
                <form id="login-form" onSubmit={handleSubmit(onSubmit)}>
                    <div className="login-container">
                    <div className="login-logo">
                        <img src="images/logo.jpg" />
                    </div>
                    <div className="login-title">
                        Log in
                    </div>
                    <div className="login-label">Username <span>*</span></div>
                    <div className="login-input">
                        <input
                            type="text"
                            name="username"
                            className="form-control"
                            autoComplete="off"
                            placeholder="Username"
                            ref={register({
                                required: true,
                                maxLength: 16,
                            })}
                        />
                        {errors.username && errors.username.type === "required" && (
                            <span className="error">Username is required</span>
                        )}
                        {errors.username && errors.username.type === "maxLength" && (
                            <span className="error">Username max length is 16 characters</span>
                        )}
                    </div>
                    <div className="login-label">Password <span>*</span></div>
                    <div className="login-input">
                        <input
                            type="password"
                            name="password"
                            className="form-control"
                            autoComplete="off"
                            placeholder="Password"
                            ref={register({
                                required: true,
                                minLength: 8,
                                pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$/,
                            })}
                        />
                        {errors.password && errors.password.type === "required" && (
                            <span className="error">Password is required</span>
                        )}
                        {errors.password && errors.password.type === "minLength" && (
                            <span className="error">Password must have min 8 characters</span>
                        )}
                        {errors.password && errors.password.type === "pattern" && (
                            <span className="error">
                                        Password must have one character in upper case, one normal case, and one special character
                                    </span>
                        )}
                    </div>
                    <div className="forgot-password">
                        <a href="#">Forgot password?</a>
                    </div>
                    <div className="create-account">
                        <a href="#">Create an account.</a>
                    </div>
                    <div className="cb"></div>
                    <div className="button-box-sign-in button-color-1">
                        <input type="submit" value="Sign in"></input>
                    </div>
                    <div className="button-line-hr"></div>
                    <div className="button-box button-color-2">
                        <svg width="32" height="23" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="comment-dots"
                             className="svg-inline--fa fa-comment-dots fa-w-16" role="img"
                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                            <path  fill="#3C1E1E"
                                  d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32zM128 272c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 0c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 0c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path>
                            </svg>
                            <span>Sign in with Kakao</span>
                    </div>
                    <div className="button-box button-color-3">
                        <img width="32" height="32" src="images/ic-naver-white.png" />
                        <span>Sign in with Naver</span>
                    </div>
                    <div className="button-box button-color-4">
                        <svg width="32" height="23" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-square"
                             className="svg-inline--fa fa-facebook-square fa-w-14" role="img"
                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                            <path fill="currentColor"
                                  d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"></path>
                        </svg>
                        <span>Sign in with Facebook</span>
                    </div>
                    <div className="button-box button-color-5">
                        <svg width="32" height="23" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="google"
                             className="svg-inline--fa fa-google fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 488 512">
                            <path fill="currentColor"
                                  d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"></path>
                        </svg>
                        <span>Sign in with Google</span>
                    </div>
                    <div className="button-box button-color-6">
                        <svg width="32" height="23"  aria-hidden="true" focusable="false" data-prefix="fab" data-icon="apple"
                             className="svg-inline--fa fa-apple fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 384 512">
                            <path fill="currentColor"
                                  d="M318.7 268.7c-.2-36.7 16.4-64.4 50-84.8-18.8-26.9-47.2-41.7-84.7-44.6-35.5-2.8-74.3 20.7-88.5 20.7-15 0-49.4-19.7-76.4-19.7C63.3 141.2 4 184.8 4 273.5q0 39.3 14.4 81.2c12.8 36.7 59 126.7 107.2 125.2 25.2-.6 43-17.9 75.8-17.9 31.8 0 48.3 17.9 76.4 17.9 48.6-.7 90.4-82.5 102.6-119.3-65.2-30.7-61.7-90-61.7-91.9zm-56.6-164.2c27.3-32.4 24.8-61.9 24-72.5-24.1 1.4-52 16.4-67.9 34.9-17.5 19.8-27.8 44.3-25.6 71.9 26.1 2 49.9-11.4 69.5-34.3z"></path>
                        </svg>
                        <span>Sign in with Apple</span>
                    </div>

                    <div className="create-new-account">
                        By clicking 'Create account' you confirm you accept our
                    </div>
                    <div className="create-new-account">
                        <a href="#">Terms of Use</a> and <a href="#">Privacy</a>
                    </div>
                </div>
                </form>
            </div>
            <div className="login-right">
                <img src="images/login-bg.png" />
            </div>
            <div className="cb"></div>
        </div>
    );
}

const mapStateToProps = (state) => ({
    isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps)(LoginView));
