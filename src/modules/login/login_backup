/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../../core/app.store";
import { useForm } from "react-hook-form";
import Cookies from 'universal-cookie';
import "./login.scss";

function LoginView(props) {
    const { register,handleSubmit,errors} = useForm({ mode: "onBlur" });
    const cookies = new Cookies();
    const onSubmit = (data) => {
        cookies.set('userInfo', data);
        props.history.push('/home');
    };

    return (
        <div>
            <div className="login-border-1">
                <div className="login-title">
                    Log In to E-Learning Course..
                </div>
                <div id="login-box" className="login-box">
                    <div className="login-note">
                        Students Enrolled in the Whisung-Chung Dahm E-Learning course can Log In Here. Log in now using your ID and password.
                    </div>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="login-border">
                            <div className="login-info">
                                <div className="login-line">Username<span className="error-asterisk">*</span></div>
                                <div className="login-line">
                                    <input
                                        type="text"
                                        name="username"
                                        className="form-control"
                                        autoComplete="off"
                                        placeholder="Username"
                                        ref={register({
                                            required: true,
                                            maxLength: 16,
                                        })}
                                    />
                                    {errors.username && errors.username.type === "required" && (
                                        <span className="error">Username is required</span>
                                    )}
                                    {errors.username && errors.username.type === "maxLength" && (
                                        <span className="error">Username max length is 16 characters</span>
                                    )}
                                </div>
                                <div className="login-line">Password <span className="error-asterisk">*</span></div>
                                <div className="login-line">
                                    <input
                                        type="password"
                                        name="password"
                                        className="form-control"
                                        autoComplete="off"
                                        placeholder="Password"
                                        ref={register({
                                            required: true,
                                            minLength: 8,
                                            pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$/,
                                        })}
                                    />
                                    {errors.password && errors.password.type === "required" && (
                                        <span className="error">Password is required</span>
                                    )}
                                    {errors.password && errors.password.type === "minLength" && (
                                        <span className="error">Password must have min 8 characters</span>
                                    )}
                                    {errors.password && errors.password.type === "pattern" && (
                                        <span className="error">
                                        Password must have one character in upper case, one normal case, and one special character
                                    </span>
                                    )}
                                </div>
                            </div>
                            <div className="login-button">
                                <input type="submit" value="Login" />
                            </div>
                            <div className="cb"></div>
                        </div>
                    </form>
                </div>
            </div>
            <div className="login-border-2">
            </div>
        </div>
    );
}

const mapStateToProps = (state) => ({
    isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps)(LoginView));
