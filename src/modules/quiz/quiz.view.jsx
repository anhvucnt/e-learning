/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../../core/app.store";
import {
  getUserInfo,
} from "../../utils/configuration";

import "./quiz.scss";

function QuizView(props) {
  const { showLoading } = props;

    const isLogin = getUserInfo() ? true : false;
    const [classInfo, setClassInfo] = useState({});

    useEffect(() => {
        onGetData();
    }, []);

    const onGetData = () => {
        showLoading(true);
        Promise.all([
            getClassInfo(),
        ])
            .then((res) => {
                showLoading(false);
            })
            .catch((err) => {
                showLoading(false);
            });
    };

    const getClassInfo = () => {
        return new Promise((resolve,reject) => {
            const myClass = {
                'Program': 'TESOL with TEC (190 hours)',
                'Period': '2020-05-11 ~ 2020-08-03',
                'Class_Weeks':'12 Weeks',
                'ClassList': [
                    {
                        'ClassName':'TESOL Module 01. Introduction',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 02. The Communicative Approach and Lesson Plan',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 03. Role-Plays and Information Gap',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 04. Teaching Reading',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 05. Teaching Writing',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 05. Teaching Writing',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 06. Teaching Grammar',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 07. Culture in the Class',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 08. Teaching Idioms',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 09. General tips for Teaching English',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TESOL Module 10. Teaching Resume',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 01. Phonics Approach to Teaching Reading',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 02. Whole Language Approach to Teaching Reading',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 03. Child Language Acquisition',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 04. The Classroom Environment',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    },
                    {
                        'ClassName':'TEC Module 05. Classroom Management',
                        'QuizUrl':'http://whisung-chung.com/school/CLASS_BOOK/module_01.pdf',
                        'Class_Start':'2020-05-11',
                        'Class_End':'2020-09-11',
                        'Quiz_Date':'2020-08-18',
                        'Quiz_Score':'3/10',
                    }
                ]
            }
            setClassInfo(myClass);
            resolve(myClass);
        })
    }

    const getTableRow = (item) => {
        const getPopupInfo = (item) => {
            return "javascript:popup_window('"+item.QuizUrl+"','QUIZ','left=0, top=0, width=800, height=550, scrollbars=2,status=0,resizable=0');";
        }
        return (
            <tr>
                <td className='lian_green_l lian_green_b '>
                    {item.ClassName}
                </td>
                <td className='lian_green_b'>{item.Class_Start}</td>
                <td className='lian_green_b'>{item.Class_End}</td>
                <td className='lian_green_b'>{item.Quiz_Date}</td>
                <td className='lian_green_b'>
                    <a href={getPopupInfo(item)}>
                        <img src="images/btn_quiz.gif"/>
                    </a>
                </td>
                <td className='lian_green_b lian_green_r'>{item.Quiz_Score}</td>
            </tr>
        )
    }

  return (
      <table width='100%' cellPadding="10" cellSpacing="0" border="0">
          <tr>
              <td>
                  <table width="100%" cellPadding='5' cellSpacing='0' className="my-class-box-1">
                      <tr>
                          <td height="35" className="cgray">Home &gt; My Class &gt; Quiz</td>
                      </tr>
                  </table>
                  <div className="sub_title my-class-box">Quiz</div>
                  {classInfo  && classInfo.ClassList && (
                  <table width="100%" align="center" cellPadding="10" cellSpacing="0">
                      <tr>
                          <td>
                              <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right lian_top table_blue">
                                          <b>Program</b></td>
                                      <td className="lian_bottom  lian_top lian_right">
                                          {classInfo.Program}
                                      </td>
                                  </tr>
                              </table>
                              <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right table_blue">
                                          <b>Period </b></td>
                                      <td className="lian_bottom lian_right">{classInfo.Period}</td>
                                  </tr>
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right table_blue"><b>Class
                                          Weeks </b></td>
                                      <td className="lian_bottom lian_right">{classInfo.Class_Weeks}</td>
                                  </tr>
                                  <tr>
                                      <td width="150" className="lian_left lian_bottom lian_right table_blue">
                                          <b>Class </b></td>
                                      <td className="lian_bottom lian_right">
                                          <table width='100%' cellPadding='7' cellSpacing='0' align="center">
                                              <tr>
                                                  <td className="table_green_t"><b>ClassName</b></td>
                                                  <td className="table_green_t"><b>Class Start</b></td>
                                                  <td className="table_green_t"><b>Class End</b></td>
                                                  <td className="table_green_t"><b>Quiz Date</b></td>
                                                  <td className="table_green_t"><b>Report</b></td>
                                                  <td className="table_green_t"><b>Score</b></td>
                                              </tr>

                                              {classInfo.ClassList && (
                                                  classInfo.ClassList.map((item,index) => {
                                                      return getTableRow(item);
                                                  })
                                              )}
                                          </table>
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>
                  )}
              </td>
          </tr>
      </table>
  );
}

const mapStateToProps = (state) => ({
    isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps)(QuizView));
