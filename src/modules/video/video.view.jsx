/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../../core/app.store";
import {
  getUserInfo,
} from "../../utils/configuration";


import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./video.scss";

function VideoView(props) {
  const { showLoading } = props;

	const isLogin = getUserInfo() ? true : false;
	const [classInfo, setClassInfo] = useState({});

	useEffect(() => {
		onGetData();
	}, []);

	const onGetData = () => {
		showLoading(true);
		Promise.all([
			getClassInfo(),
		])
			.then((res) => {
				showLoading(false);
			})
			.catch((err) => {
				showLoading(false);
			});
	};

	const getClassInfo = () => {
		return new Promise((resolve,reject) => {
			const myClass = {
				'Program': 'TESOL with TEC (190 hours)',
				'Period': '2020-05-11 ~ 2020-08-03',
				'Class_Weeks':'12 Weeks',
				'Register_Date':'2020-05-11',
				'TotalView':'512K',
				'ClassList': [
					{
						'ClassName':'TESOL Module 01. Introduction',
						'Class_Start':'2020-05-11',
						'Class_End':'2020-09-11',
						'View':'1k',
					},
					{
						'ClassName':'TESOL Module 02. The Communicative Approach and Lesson Plan',
						'Class_Start':'2020-05-11',
						'Class_End':'2020-09-11',
						'View':'2k',
					},
					{
						'ClassName':'TESOL Module 03. Role-Plays and Information Gap',
						'Class_Start':'2020-05-11',
						'Class_End':'2020-09-11',
						'View':'3k',
					},
					{
						'ClassName':'TESOL Module 04. Teaching Reading',
						'Class_Start':'2020-05-11',
						'Class_End':'2020-09-11',
						'View':'3k',
					},
					{
						'ClassName':'TESOL Module 05. Teaching Writing',
						'Class_Start':'2020-05-11',
						'Class_End':'2020-09-11',
						'View':'3k',
					}
				]
			}
			setClassInfo(myClass);
			resolve(myClass);
		})
	}

  return (
      <table width='100%' cellpadding="10" cellspacing="0" border="0" >
			<tr>
				<td>
					<table width="100%"  cellpadding='5' cellspacing='0' className="my-class-box-1">
						<tr>
							<td height="35" class="cgray">Home &gt; My Class &gt; Online Video Class</td>
						</tr>
					</table>
					<table width="100%" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td valign="center">
								<div class="sub_title my-class-box">Online Video Class</div>
							</td>
							<td align="right">
							</td>
						</tr>
					</table>
					{classInfo  && classInfo.ClassList && (
						<div>
					<table width='100%' cellpadding='7' cellspacing='0' align="center">
						<tr>
							<td width="150" class="lian_left lian_bottom lian_right lian_top table_blue"><b>Program</b></td>
							<td class="lian_bottom  lian_top lian_right table_blue">
								{classInfo.Program}
							</td>
						</tr>
					</table>
					<table width='100%' cellpadding='7' cellspacing='0' align="center">
						<tr>
							<td width="150" class="lian_left lian_bottom lian_right table_blue"><b>Period </b></td>
							<td class="lian_bottom lian_right">{classInfo.Period}</td>
						</tr>
						<tr>
							<td width="150" class="lian_left lian_bottom lian_right table_blue"><b>Class Weeks  </b></td>
							<td class="lian_bottom lian_right">{classInfo.Class_Weeks}</td>
						</tr>
						<tr>
							<td width="150" class="lian_left lian_bottom lian_right table_blue"><b>Class </b></td>
							<td class="lian_bottom lian_right">
								<table width='100%' cellpadding='7' cellspacing='0' align="center" >
									<tr>
										<td class="table_green_t"><b>ClassName</b></td>
										<td class="table_green_t"><b>Class Start</b></td>
										<td class="table_green_t"><b>Class End</b></td>
										<td class="table_green_t"><b>View({classInfo.TotalView})</b></td>
									</tr>
									{classInfo.ClassList && (
										classInfo.ClassList.map((item,index) => (
											<tr id={index}>
												<td class='lian_green_l lian_green_b '>{item.ClassName}</td>
												<td class='lian_green_b'>{item.Class_Start}</td>
												<td class='lian_green_b'>{item.Class_End}</td>
												<td algin='right' class='lian_green_b lian_green_r'>{item.View}</td>
											</tr>
										))
									)}
								</table>
							</td>
						</tr>
					</table>
						</div>
					)}
				</td>
			</tr>
		</table>
  );
}

const mapStateToProps = (state) => ({
	isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			showLoading: appActions.ShowLoading,
		},
		dispatch
	);

export default (connect(mapStateToProps, mapDispatchToProps) (VideoView));
