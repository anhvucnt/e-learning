import React from "react";
import HeaderView from "../header/header.view.jsx";
import FooterView from "../footer/footer.view.jsx";

import "./layout.scss";

export default class LayoutDesktopView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bodyHeight: null,
    };
  }

  componentDidMount() {

  }

  render() {
    return (
        <div style={this.state.bodyHeight}>{this.props.children}</div>
    );
  }
}
