import React from "react";
import HeaderView from "../header/header.view.jsx";
import MenuView from "../menu/menu.view.jsx";
import FooterView from "../footer/footer.view.jsx";
import LoadingWithApiQueue from "../loading/loading-with-queue.view";

import "./layout.scss";
import WelcomeView from "../welcome/welcome.view";

export default class LayoutDesktopView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bodyHeight: null,
    };
  }

  componentDidMount() {

  }

  render() {
    return (
        <table width='100%' cellspacing="0" cellpadding="0" border="0" bgcolor="#929ae6" height="100%" className="main-padding">
      <tr>
          <td valign="top">
              <HeaderView match={this.props.match} />
              <table cellpadding="5" cellspacing="0" border="0" align="center" className="content-margin">
              <tr>
                  <td width="220" bgcolor="white" align="center" valign="top">
                  <WelcomeView match={this.props.match} />
                  <MenuView match={this.props.match} />
                  <LoadingWithApiQueue />
          </td>
          <td width="5"></td>
      <td valign="top" bgcolor="ffffff">
          <div style={this.state.bodyHeight}>{this.props.children}</div>
      </td>
  </tr>
  </table>
      <div className="h5"></div>
      <div className="cb"></div>
              <FooterView />
  </td>
  </tr>
  </table>
    );
  }
}
