/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useRef, useEffect } from "react";
import { useForm } from "react-hook-form";
import "./menu.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ViLanguage from "../../languages/vi";
import {
  DomainUserSide,
  DomainAdminSide,
  APIUrlDefault,
  TokenKey,
  getUserInfo,
  removeCookies,
} from "../../utils/configuration";
import * as appActions from "../../core/app.store";

class MenuView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: getUserInfo() ? true : false,
      infoAcount: null
    };
  }

  componentWillMount() {

  }

  render() {
    return (
        <table width="100%" border="0" cellSpacing="0" cellPadding="0">
          <tr>
            <td height="35" valign="middle" background="images/menu_bg01.gif" className="menu-border-1">
              <font className="menu-color-1">&nbsp;<img src="images/folder.gif" align='absmiddle' />&nbsp;&nbsp;
                <strong>MY INFORMATION</strong></font>
            </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellSpacing="0" cellPadding="5">
                <tr>
                  <td height="35" className="menu-line-border">
                    &nbsp;<img src="images/member.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/profile">My
                    Information</a></td>
                </tr>
              </table>
            </td>
          </tr>
            <tr>
              <td height="35" valign="middle" background="images/menu_bg01.gif">
                <font className="menu-color-1">&nbsp;<img src="images/folder.gif" align='absmiddle' />&nbsp;&nbsp;
                  <strong>My Class</strong></font>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellSpacing="0" cellPadding="5">
                  <tr>
                    <td height="35" className="menu-line-border" valign="center">
                      &nbsp;<img src="images/qa.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/home">Main</a>
                    </td>
                  </tr>
                  <tr>
                    <td height="35" className="menu-line-border" valign="center">
                      &nbsp;<img src="images/video.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/video">Video
                      Class</a></td>
                  </tr>
                  <tr>
                    <td height="35" className="menu-line-border">
                      &nbsp;<img src="images/textbook.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/book"> Textbook</a></td>
                  </tr>
                  <tr>
                    <td height="35" className="menu-line-border">
                      &nbsp;<img src="images/quiz.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/quiz"> Quiz</a>
                    </td>
                  </tr>
                  <tr>
                    <td height="35" className="menu-line-border">
                      &nbsp;<img src="images/assign.gif" align='absmiddle' />&nbsp;&nbsp;<a href="assign"> Assignment</a></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="35" valign="middle" background="images/menu_bg01.gif">
                <font className="menu-color-1">&nbsp;<img src="images/folder.gif" align='absmiddle' />&nbsp;&nbsp;
                  <strong>Q&A and Notice</strong></font>
              </td>
            </tr>
            <tr>
              <td>
                <table width="100%" border="0" cellSpacing="0" cellPadding="5">
                  <tr>
                    <td height="35" className="menu-line-border">
                      &nbsp;<img src="images/qa.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/qna">Q&A Regarding
                      Course</a></td>
                  </tr>

                  <tr>
                    <td height="35" className="menu-line-border">
                      &nbsp;<img src="images/qa.gif" align='absmiddle' />&nbsp;&nbsp;<a href="/board">Webinar/Class
                      Notice</a></td>
                  </tr>
                </table>
              </td>
            </tr>
        </table>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      showLoading: appActions.ShowLoading,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MenuView);
