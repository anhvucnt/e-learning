import React from "react";
import "./footer.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMapMarkedAlt,
  faPhoneAlt,
  faEnvelope,
} from "@fortawesome/free-solid-svg-icons";
import Vilanguage from "../../languages/vi";

const LanguageDisplay = Vilanguage;

export default class FooterView extends React.Component {
  render() {
    return (
        <div className="footer-copyright">
            Copyright ⓒ <a href="#">mysite.com</a> . All rights reserved.
        </div>
    );
  }
}
