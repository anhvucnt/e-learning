/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../../core/app.store";
import Cookies from 'universal-cookie';
import "./welcome.scss";
import {useForm} from "react-hook-form";

function WelcomeView (props) {
  const cookies = new Cookies();
  const { register,handleSubmit,errors} = useForm({ mode: "onBlur" });
  const getUserInfo = () => {
    try{
      let userInfo = cookies.get('userInfo');
      if(userInfo && userInfo.username){
        return userInfo.username;
      }else{
        cookies.remove('userInfo');
        window.location.replace('/');
      }
    }catch (e) {
      cookies.remove('userInfo');
      window.location.replace('/');
    }
  }

  const onSubmit = (data) => {
    cookies.remove('userInfo');
    window.location.replace('/');
  };

    return (
        <table width="175" border="0" cellPadding="0" cellSpacing="0">
          <tr>
            <td valign="top">
              <table width="100%" border="0" cellSpacing="0" cellPadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellSpacing="0" cellPadding="0">

                      <tr>

                        <td width="20"></td>
                        <td>
                          <table width="175" border="0" cellSpacing="0" cellPadding="0">
                            <tr>
                              <td height="12" colSpan="3"></td>
                            </tr>
                            <tr>
                              <td colSpan="3">
                                <div><font color="#747474">Welcome</font>
                                  &nbsp;
                                  <b>{getUserInfo()}</b>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td height="10" colSpan="3"></td>
                            </tr>

                            <tr>
                              <td width="4">&nbsp;</td>
                              <td width="91" align="left">
                                <form onSubmit={handleSubmit(onSubmit)}>
                                  <input className="logout-button" type="submit" value="Log out" />
                                </form>
                              </td>
                            </tr>
                            <tr>
                              <td height="10" colSpan="3"></td>
                            </tr>

                            <tr>
                              <td height="8" colSpan="3"></td>
                            </tr>
                          </table>
                        </td>
                        <td width="20"></td>
                      </tr>
                    </table>
                  </td>
                </tr>

              </table>
            </td>
          </tr>
        </table>

    );
}

const mapStateToProps = (state) => ({
  isLoading: state.app.loading,
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
          showLoading: appActions.ShowLoading,
        },
        dispatch
    );

export default (connect(mapStateToProps, mapDispatchToProps) (WelcomeView));
