import { combineReducers } from "redux";
import AppReducer from "../core/app.store";
import LoadingReducer from "../redux/store/loading/loading.store";

export default combineReducers({
  app: AppReducer,
  loadingState: LoadingReducer,
});
