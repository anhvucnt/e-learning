import Service from "../../../api/api-service";
import { ApiUrl } from "../../../api/api-url";

const service = new Service();

export const GetDetailProfile = (id) => {
  let requestUrl = ApiUrl.GetDetailProfile + '/' +id;

  return service
    .get(requestUrl)
    .then((res) => {
      return res;
    })
    .catch((err) => {
      throw err;
    });
};


