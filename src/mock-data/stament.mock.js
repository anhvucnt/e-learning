export const ListStaments = {
  ServicePortal: [
    {
      title: "title1",
      link: "link1",
      order: "order1",
      image: "image1",
    },
    {
      title: "title2",
      link: "link2",
      order: "order2",
      image: "image2",
    },
    {
      title: "title3",
      link: "link3",
      order: "order3",
      image: "image3",
    },
  ],
  Parents: [
    {
      Title: "title1",
      Order: "order1",
      Childs: [
        {
          title: "title1",
          link: "link1",
          order: "order1",
          image: "image1",
        },
        {
          title: "title2",
          link: "link2",
          order: "order2",
          image: "image2",
        },
        {
          title: "title3",
          link: "link3",
          order: "order3",
          image: "image3",
        },
      ]
    },
    {
      Title: "title 2",
      Order: "order2",
      Childs: [
        {
          title: "title1",
          link: "link10",
          order: "order1",
          image: "image1",
        },
        {
          title: "title2",
          link: "link20",
          order: "order2",
          image: "image2",
        },
        {
          title: "title3",
          link: "link30",
          order: "order3",
          image: "image3",
        },
      ]
    }
  ]
}