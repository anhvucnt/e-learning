export const ListObjectSearch = [
    {
        name: "Mạng lưới đường",
        attributes: [
            {
                name: "Ký hiệu mặt cắt ngang",
                type: "dropdown",
                placeholder: "Chọn Ký hiệu mặt cắt ngang",
                values: [
                    {
                        name: "5H-5H",
                        value: "5h"
                    },
                    {
                        name: "6H-7H",
                        value: "5h"
                    },
                    {
                        name: "5H-5H",
                        value: "5h"
                    },
                ]
            },
            {
                name: "Bề rộng mặt cắt ngang",
                type: "inputNumber",
                placeholder: "Nhập độ rộng"
            },
            {
                name: "Chú thích",
                type: "input",
                placeholder: "Nhập chú thích"
            },
        ]
    },
    {
        name: "Mạng lưới bắt cá",
        attributes: [
            {
                name: "Ký hiệu mặt cắt ngang",
                type: "dropdown",
                placeholder: "Chọn Ký hiệu mặt cắt ngang",
                values: [
                    {
                        name: "5H-5H",
                        value: "5h"
                    },
                    {
                        name: "6H-7H",
                        value: "5h"
                    },
                    {
                        name: "5H-5H",
                        value: "5h"
                    },
                ]
            },
            {
                name: "Bề rộng mặt cắt ngang",
                type: "inputNumber",
                placeholder: "Nhập độ rộng"
            },
            {
                name: "Chú thích",
                type: "input",
                placeholder: "Nhập chú thích"
            },
        ]
    },
]