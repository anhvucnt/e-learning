export const ResultCommunityFake = {
  listResult: [
    {
      Id: 1,
      PlanningName:
        "Quy hoạch chung đô thị Đông Triều, Tỉnh Quảng Ninh đến năm 2040",
      Title:
        "Xin ý kiến cộng đồng về Quy hoạch chung đô thị Thanh Hóa, tỉnh Thanh Hóa đến năm 2040",
      StartTime: "01/06/2020",
      EndTime: "30/07/2020",
      imageUrl: "/static/media/demo-thumbnail.33134ce8.jpg",
    },
    {
      Id: 2,
      PlanningName:
        "Quy hoạch chung đô thị Đông Triều, Tỉnh Quảng Ninh đến năm 2040",
      Title:
        "Xin ý kiến cộng đồng về Quy hoạch chung đô thị Thanh Hóa, tỉnh Thanh Hóa đến năm 2040",
      StartTime: "01/06/2020",
      EndTime: "30/07/2020",
      imageUrl: "/static/media/demo-thumbnail.33134ce8.jpg",
    },
    {
      Id: 3,
      PlanningName:
        "Quy hoạch chung đô thị Đông Triều, Tỉnh Quảng Ninh đến năm 2040",
      Title:
        "Xin ý kiến cộng đồng về Quy hoạch chung đô thị Thanh Hóa, tỉnh Thanh Hóa đến năm 2040",
      StartTime: "01/06/2020",
      EndTime: "30/07/2020",
      imageUrl: "/static/media/demo-thumbnail.33134ce8.jpg",
    },
  ],
};
