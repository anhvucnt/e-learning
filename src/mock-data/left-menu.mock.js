export const mockData = [
    {
        groupName: "Ranh giới quy hoạch",
        listItems: [{
            name: "Ranh giới quy hoạch"
        },
        {
            name: "Ranh giới quy hoạch"
        }]
    }, {
        groupName: "Quy hoạch sử dụng đất",
        listItems: []
    },
]

export const mockData1 = [
    {
        label: "Loại quy hoạch",
        value: "QHC"
    },
    {
        label: "Tên đồ án",
        value: "Quy hoạch chung đô thị Đông Triều, tỉnh Đông Triều đến năm 2040"
    },
    {
        label: "Nhu cầu dùng điện",
        value: ""
    },
    {
        label: "Số lượng thuê bao",
        value: "120"
    },
]

export const mockData2 = [
    {
        groupName: "Quy hoạch lân cận",
        listItems: [{
            name: "Phân khu 7",
            value: "Phân khu 7",
        },
        {
            name: "Phân khu 8",
            value: "Phân khu 8",
        },
        {
            name: "Phân khu 9",
            value: "Phân khu 9",
        },
        ]
    },
    {
        groupName: "Quy hoạch cấp trên",
        listItems: [{
            name: "QHC Thị xã Đông Triều",
            value: "QHC Thị xã Đông Triều",
        },
        ]
    },
]