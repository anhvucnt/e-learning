export const HomePage = [
    {
      avatar: "dwh/coms/report/getResource.ashx?key=f09a-5c95-7a9a-1089-99d4.png&owner=bfaa574b-2e0e-4632-a0f7-0918c147771b",
      introduce: "&lt;p&gt;Toàn bộ địa giới hành chính thị xã Đông Triều và huyện Đông Sơn. Quy mô diện tích khoảng 232,64km2 và quy mô dân số khoảng 512.500 người. Là đô thị loại I trực thuộc TW, là một trong những trung tâm kinh tế, dich vụ, chăm sóc sức khỏe, giáo dục đào tạo, thể dục thể thao của vùng phía Nam Bắc Bộ và Bắc Trung Bộ, có vị thế quan trọng về an ninh quốc phòng.&lt;/p&gt;",
      status: 1,
      id: 1,
      updateOn: "2020-03-08T22:14:45.562962",
      order: 1,
      title: "QHC ĐÔ THỊ Đông Triều, TỈNH Đông Triều ĐẾN NĂM 2040"
    },
    {
      avatar: "dwh/coms/report/getResource.ashx?key=f09a-5c95-7a9a-1089-99d4.png&owner=bfaa574b-2e0e-4632-a0f7-0918c147771b",
      introduce: "&lt;p&gt;Toàn bộ địa giới hành chính thị xã Đông Triều và huyện Đông Sơn. Quy mô diện tích khoảng 232,64km2 và quy mô dân số khoảng 512.500 người. Là đô thị loại I trực thuộc TW, là một trong những trung tâm kinh tế, dich vụ, chăm sóc sức khỏe, giáo dục đào tạo, thể dục thể thao của vùng phía Nam Bắc Bộ và Bắc Trung Bộ, có vị thế quan trọng về an ninh quốc phòng.&lt;/p&gt;",
      status: 1,
      id: 2,
      updateOn: "2020-03-08T22:14:45.562962",
      order: 1,
      title: "QHC ĐÔ THỊ Đông Triều, TỈNH Đông Triều ĐẾN NĂM 2040"
    }
  ]