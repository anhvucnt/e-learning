export const ApiUrl = {
  //--- Account
  Login: "api/Account/Login",
  Register: "api/Account/Register",
  ForgotPassword: "api/Account/ForgotPassword",
  ResetPassword: "api/Account/ResetPassword",
  ContactToAdmin: "api/cms/HomePage/ContactToAdmin",
  GetUserAccountDetail: "api/Account/GetUserAccountDetail",
  UpdateUserAccount: "api/Account/UpdateUserAccount",

  //Home Page
  SlideShow: "api/cms/HomePage/GetListPostHomePage",

  //--- News
  GetDetailProfile: "api/cms/profile/GetDetailProfile",

};
