const env = process.env.NODE_ENV || "development";

const apiEnvironment = {
  development: {
        api: "https://caobang-api.cgis.asia/",
  },
  production: {
    api: "https://caobang-api.cgis.asia/",
  },
};

module.exports = apiEnvironment[env];
