# Prerequirements

1. NodeJs v14.15.0
2. ReactJs v17.0.1
3. IDE example Visual Studio, Notepad++, phpStorm, ...
4. Make sure port 3000 is available

# Installing

1. Clone source code form github: 
2. Go to project folder, and run: npm i
3. Start application, run: npm start
4. Login with account: admin/Admin@123456

# Deploy source code to production server

1. Go to project folder, and run: npm run build, wait a few minutes until success. 
2. You will see a folder named "build" appear at root of your project
3. Copy all of files in "build" folder to your hosting / server for deploy project to live server


